import { io } from "https://cdn.socket.io/4.4.1/socket.io.esm.min.js";
            
var socket;

export function connect() {
    socket = io("ws://localhost:7645");
}

export async function wait_for_connection(depth=0) {
    return new Promise(async(resolve, reject) => {
        while(socket == undefined) continue;
        if(socket.connected) resolve();
        else if(depth < 10) await wait_for_connection(depth+1);
        else reject();
    })
}

export async function get_packages() {
    return new Promise((resolve) => {    
        socket.emit("command", "list");
        socket.on("data", d => {
            if(d.split("\n")[0] == "<TYPE_LIST>") {
                resolve(d.replace(d.split("\n")[0] + "\n", '').split("\n"));
            }
        })
    })
}

export async function install_package(pkg) {
    return new Promise(resolve => {
        socket.emit("command", "install " + pkg + " -y");
        socket.on("data", msg => {
            if(msg.split("\n")[0] == "<TYPE_INSTALL>") {
                resolve(msg.includes("# Done installing " + pkg + "!"));
            }
        })
    })
}

export async function uninstall_package(pkg) {
    return new Promise(resolve => {
        socket.emit("command", "uninstall " + pkg + " -y");
        socket.on('data', msg => {
            if(msg.split("\n")[0] == "<TYPE_UNINSTALL>") {
                resolve(msg.includes("# Uninstalled " + pkg))
            }
        })
    })
}

export function listen() {
    socket.on("connect", () => {
        console.log("Connected")
    })
    
    socket.on("error", (e) => {
        console.warn("Could not connect to websocket: ", e);
    })
}