function search_pkgs(text) {
    window.location = "/packages/?search=" + text
}

window.addEventListener("keypress", function(e) {
    if(e.key == "Enter" && document.activeElement.id == "searchbar") {
        search_pkgs(document.getElementById("searchbar").value)
    }
})