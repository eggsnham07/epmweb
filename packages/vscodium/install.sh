#!/bin/bash
function failed() {
	echo $1
	exit 1
}

export VSCODIUM_VER=1.96.2.24355
export VSCODIUM_URL=https://github.com/VSCodium/vscodium/releases/download/$VSCODIUM_VER/VSCodium-linux-x64-$VSCODIUM_VER.tar.gz

echo "Unoffical VSCodium package for EPM"
if ! [[ -d "/home/$USER/.local/bin/VSCodium" ]]; then
        echo ""
        echo "Creating directories..."
        mkdir -p ~/.local/bin/VSCodium || failed "Could not create dir '/home/$USER/.local/bin/VSCodium'"
fi

if ! [[ -d "/home/$USER/.local/bin/res" ]]; then
	mkdir -p ~/.local/bin/res || failed "Could not creat dir '/home/$USER/.local/bin/res'"
fi
echo ""
echo "Running wget to download app"
wget -q --show-progress $VSCODIUM_URL -P /tmp
echo ""
echo "Downloading launcher icon..."
wget -q --show-progress https://raw.githubusercontent.com/VSCodium/vscodium/master/icons/stable/codium_cnl.svg -P /home/$USER/.local/bin/res
mv /home/$USER/.local/bin/res/code.png /home/$USER/.local/bin/res/vscodium.png
echo ""
echo "Using 'tar -xf' to extract tar archive, this might take a while..."
tar -xf /tmp/VSCodium-linux-x64-$VSCODIUM_VER.tar.gz -C ~/.local/bin/VSCodium > /dev/null || failed "Could not run tar -xf on the tar archive!"
echo ""
echo "Creating application menu launcher..."
echo "[Desktop Entry]" > ~/.local/share/applications/vscodium.desktop
echo "Name=VSCodium" >> ~/.local/share/applications/vscodium.desktop
echo "Exec=/home/$USER/.local/bin/VSCodium/bin/codium" >> ~/.local/share/applications/vscodium.desktop
echo "Icon=/home/$USER/.local/bin/res/codium_cnl.svg" >> ~/.local/share/applications/vscodium.desktop
echo "Type=Application" >> ~/.local/share/applications/vscodium.desktop
echo "Terminal=false" ~/.local/share/applications/vscodium.desktop
echo "Categories=Development" >> ~/.local/share/applications/vscodium.desktop
echo ""
echo "[PLEASE READ]"
echo "You may need to run 'sudo update-desktop-database' for things to show up in your application launcher"
rm /tmp/VSCodium-linux-x64-$VSCODIUM_VER.tar.gz || echo "Could not remove /tmp/VSCodium-linux-x64-$VSCODIUM_VER.tar.gz, you will need to do it yourself"
echo ""
echo "Done!"
export VSCODIUM_URL=''
export VSCODIUM_VER=''
