#!/bin/bash
function failed() {
    echo $1
    exit 1
}

cd /tmp
git clone https://gitlab.com/eggsnham07/epm || failed "Could not clone repo!"
cd epm
bash ./setup.sh || failed "Failed to run ./setup.sh"
rm -rf /tmp/epm