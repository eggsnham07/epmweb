#!/bin/bash
function failed() {
    echo $1
    exit 1
}

export XONOTIC_VERSION='0.8.6'

echo "Unoffical Xonotic EPM Installer"
echo "[NOTE]: To run xonotic sdl you will need to install libsdl first!"
echo ""
if ! [[ -d "/home/$USER/.local/bin/res" ]]; then
    mkdir -p ~/.local/bin/res
fi
if [[ -d "/home/$USER/.local/bin/Xonotic" ]]; then
    rm -rf ~/.local/bin/Xonotic
fi

echo "Downloading zip file..."
wget -q --show-progress https://dl.unvanquished.net/share/xonotic/release/xonotic-$XONOTIC_VERSION.zip -P /tmp || failed "Could not download zip file"
echo "Downloading desktop icon..."
wget -q --show-progress https://epm.eggsnham.com/images/Xonotic.png -P ~/.local/bin/res || failed "Unable to download desktop icon"
unzip /tmp/xonotic-$XONOTIC_VERSION.zip -d ~/.local/bin
echo ""
echo "Creating application menu shortcuts..."
echo "[Desktop Entry]" > ~/.local/share/applications/xonotic-sdl.desktop
echo "Name=Xonotic SDL" >> ~/.local/share/applications/xonotic-sdl.desktop
echo "Exec=/home/$USER/.local/bin/Xonotic/xonotic-linux-sdl.sh" >> ~/.local/share/applications/xonotic-sdl.desktop
echo "Icon=/home/$USER/.local/bin/res/Xonotic.png" >> ~/.local/share/applications/xonotic-sdl.desktop
echo "Type=Application" >> ~/.local/share/applications/xonotic-sdl.desktop
echo "Terminal=true" >> ~/.local/share/applications/xonotic-sdl.desktop
echo "Categories=Game" >> ~/.local/share/applications/xonotic-sdl.desktop

echo "[Desktop Entry]" > ~/.local/share/applications/xonotic-glx.desktop
echo "Name=Xonotic GLX" >> ~/.local/share/applications/xonotic-glx.desktop
echo "Exec=/home/$USER/.local/bin/Xonotic/xonotic-linux-glx.sh" >> ~/.local/share/applications/xonotic-glx.desktop
echo "Icon=/home/$USER/.local/bin/res/Xonotic.png" >> ~/.local/share/applications/xonotic-glx.desktop
echo "Type=Application" >> ~/.local/share/applications/xonotic-glx.desktop
echo "Terminal=true" >> ~/.local/share/applications/xonotic-glx.desktop
echo "Categories=Game" >> ~/.local/share/applications/xonotic-glx.desktop
echo ""
echo "[PLEASE READ]"
echo "You may need to run 'sudo update-desktop-database' to find your application in the app menu"
echo ""
echo ""
rm /tmp/xonotic-$XONOTIC_VERSION.zip || echo "Could not remove /tmp/xonotic-$XONOTIC_VERSION, you will need to remove it yourself"
echo "Done!"
