#!/bin/bash
export GODOT_VERSION=3.5.2
export GODOT_ZIP="Godot_v$GODOT_VERSION-stable_x11.64.zip"

function failed() {
	echo "$1"
	exit 1
}

echo ""
echo "Creating directories"
if ! [[ -d "/home/$USER/.local/bin/res" ]]; then
	mkdir -p ~/.local/bin/res > /dev/null && echo "Created /home/$USER/.local/bin" || failed "Could not create dir '/home/$USER/.local/bin"
fi
if ! [[ -d "/home/$USER/.local/share/applications" ]]; then
	mkdir -p ~/.local/share/applications && echo "Created /home/$USER/.local/share/applications" || failed "Could not reate dir '/home/$USER/.local/share/applications'"
fi
if [[ -f "/home/$USER/.local/bin/Godot_v$GODOT_VERSION-stable_x11.64" ]]; then
	echo ""
	echo "Removing previous install"
	rm ~/.local/bin/Godot_v$GODOT_VERSION-stable_x11.64 && echo "Removed." || failed "Unable to remove /home/$USER/.local/bin/Godot_v$GODOT_VERSION-stable_x11.64"
fi

echo ""
echo "Downloading executable and icon"
wget -q --show-progress https://github.com/godotengine/godot/releases/download/$GODOT_VERSION-stable/$GODOT_ZIP -P /tmp || failed "Failed to run wget for executable"
wget -q --show-progress https://git.eggsnham.com/content/Godot.png -P /tmp || failed "Failed to run wget for desktop icon"

echo ""
echo "Unzipping"
unzip /tmp/$GODOT_ZIP -d ~/.local/bin || failed "Could not unzip /tmp/$GODOT_ZIP"
mv /tmp/Godot.png ~/.local/bin/res
echo ""
echo "Creating desktop file for application menu"
echo "[Desktop Entry]" >> ~/.local/share/applications/godot-lts.desktop
echo "Name=Godot Engine" >> ~/.local/share/applications/godot-lts.desktop
echo "Exec=/home/$USER/.local/bin/Godot_v$GODOT_VERSION-stable_x11.64" >> ~/.local/share/applications/godot-lts.desktop
echo "Icon=/home/$USER/.local/bin/res/Godot.png" >> ~/.local/share/applications/godot-lts.desktop
echo "Type=Application" >> ~/.local/share/applications/godot-lts.desktop
echo "Terminal=false" >> ~/.local/share/applications/godot-lts.desktop
echo "Categories=Game,Development" >> ~/.local/share/applications/godot-lts.desktop
echo ""
echo "[PLEASE READ]"
echo "When this command is done you need to run 'sudo update-desktop-database' to find godot engine in you application list"
echo ""
echo "Removing tmp zip file"
rm -rf /tmp/$GODOT_ZIP || echo "Unable to remove /tmp/$GODOT_ZIP"
echo "Done installing Godot Engine executable!" 
