#!/usr/bin/node

const fs = require("fs")

const packages = fs.readdirSync("./packages")
var full_json = []

for(const package of packages) {
    if(fs.lstatSync("./packages/" + package).isDirectory()) {
        const pkg_info = fs.readFileSync("./packages/" + package + "/package.epmpkg", "utf-8")
        full_json.push({
            name: pkg_info.split("name=")[1].split("\n")[0],
            id: package,
            version: pkg_info.split("version=")[1].split("\n")[0],
            description: pkg_info.split("description=")[1].split("\n")[0],
            author: pkg_info.split("author=")[1].split("\n")[0]
        })
    }
}

fs.writeFileSync("./packages/packages.json", JSON.stringify(full_json))